﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SatelitePlayer2Script : MonoBehaviour
{
   public LaunchBase2Script player2;

   public Transform satelite;
   public Rigidbody2D sateliteRigidbody;
   private Transform planet; //you need a reference to the planet, declares which one upon collision

   private void Start()
   {
      planet = null;
   }

   void OnCollisionEnter2D(Collision2D collisionInfo)
   {
      if (collisionInfo.collider.tag == "Planet")
      {
         //Debug.Log("Detected cllission\n");
         planet = collisionInfo.transform;
         satelite.transform.parent = planet.transform;   //sets the crashed into planet as the parent of the satelite in order to initiate rotation
         //Debug.Log(satelite.transform.parent);
         sateliteRigidbody.velocity = Vector2.zero;      //cancels out the velocity of the satelite in order to keep it "in orbit"
         player2.UpdateScore2(10);
      }

      if (collisionInfo.collider.tag == "SatelitePlayer1")
      {
         collisionInfo.gameObject.SetActive(false);
         //Destroy(collisionInfo.gameObject);      // cannot destroy with the current problem of not keeping the satelite strictly as a prefab
         gameObject.SetActive(false);
         //Destroy(gameObject);
         player2.UpdateScore2(10);

      }
   }
}