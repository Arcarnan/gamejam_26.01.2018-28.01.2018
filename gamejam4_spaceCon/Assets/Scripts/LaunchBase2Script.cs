﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBase2Script : MonoBehaviour
{
   public Transform launchBase;
   public Transform planet; //you need a reference to the planet
   public GameObject satelite1;
   public GameObject satelite2;
   public GameObject satelite3;
   private GameObject satelite;

   public int player2Score;
   public static float Gravity = -9.81f;
   public float minSpeed = 5f;
   public float maxSpeed = 10f;

   private float speed = 5.0f;
   private int sateliteCost = 5;

   private void Start()
   {
      launchBase.transform.parent = planet.transform;
      player2Score = 30;   //TODO reduce
   }

   private void Update()
   {
      if (Input.GetKey("up") && speed <= maxSpeed)
      {
         speed += 5.0f * Time.deltaTime;
         //Debug.Log(speed);
      }

      if (Input.GetKeyUp("up"))
      {
         int rand = (int) RandomNumber(1, 4);
         Debug.Log(rand);
         switch (rand)
         {
            case 1:
               satelite = satelite1;
               break;

            case 2:
               satelite = satelite2;
               break;

            case 3:
               satelite = satelite3;
               break;
         }
         
         if (player2Score >= sateliteCost)   // only able to send satelites if you have the required currency
         {
         player2Score -= sateliteCost;
         Mathf.Clamp(speed, minSpeed, maxSpeed);

         Rigidbody2D sateliteInstance = Instantiate(satelite, transform.position, Quaternion.Euler(new Vector3(0, 0, 1))).GetComponent<Rigidbody2D>();
         sateliteInstance.velocity = transform.up * speed;

         Physics2D.IgnoreCollision(sateliteInstance.GetComponent<Collider2D>(), launchBase.GetComponent<Collider2D>());
         speed = minSpeed;
         }
         //TODO else: implement error, too few resources
      }
   }

   public int GetScore2()
   {
      return player2Score;
   }

   public void UpdateScore2 (int modifier)
   {
      player2Score += modifier;
   }

   static float RandomNumber(float min, float max)
   {
      float rand = Random.Range(min, max);
      return rand;
   }
}
