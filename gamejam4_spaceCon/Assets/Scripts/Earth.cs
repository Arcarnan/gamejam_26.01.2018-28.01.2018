﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//
//public class Earth : MonoBehaviour
//{
//
//    public GameObject planet;
//
//    float rotationSpeed = 5.0f;
//    public float radius; //Radius of the planets circle collider
//    private float orbitAngle; // Target angle of the orbiting object
//    private Vector3 direction;
//
//
//    // Use this for initialization
//    void Start ()
//    {
//        radius = planet.GetComponent<CircleCollider2D>().radius; // Orbiting Distance from the planet
//
//    }
//	
//	// Update is called once per frame
//	void Update ()
//    {
//        transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
//
//        // ROTATION STUFF
//        direction = (this.transform.position - planet.transform.position);
//        direction = planet.transform.InverseTransformDirection(direction);
//        orbitAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
//
//        Quaternion q = Quaternion.AngleAxis(orbitAngle, Vector3.forward);
//        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, q, Time.deltaTime * rotationSpeed);
//    }
//}
