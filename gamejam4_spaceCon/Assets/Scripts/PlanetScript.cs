﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetScript : MonoBehaviour
{
   public float gravityRadius = 2.0f;
   public float radius; //Radius of the planets circle collider
   public float speed = 17f;
   public GameObject thisPlanet;

   void Start()
   {
      radius = thisPlanet.GetComponent<CircleCollider2D>().radius; // Orbiting Distance from the planet
   }

   void Update()
   {
      transform.Rotate(Vector3.forward, speed * Time.deltaTime, Space.World);
   }

   ////for seeing the gravity radius of the planets
   //private void OnDrawGizmosSelected()
   //{
   //   Gizmos.color = Color.red;
   //   Gizmos.DrawWireSphere(transform.position, radius);
   //}
}
