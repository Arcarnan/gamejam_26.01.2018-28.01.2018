﻿using UnityEngine;
using System.Collections;

public class EarthSpinScript : MonoBehaviour
   // script from Unity asset store
{
   public float speed = 30f;
   public float radius; //Radius of the planets circle collider
   public GameObject thisPlanet;

   void Start()
   {
      radius = thisPlanet.GetComponent<CircleCollider2D>().radius; // Orbiting Distance from the planet
   }

   void Update()
   {
      transform.Rotate(Vector3.back, speed * Time.deltaTime, Space.World);
   }
}