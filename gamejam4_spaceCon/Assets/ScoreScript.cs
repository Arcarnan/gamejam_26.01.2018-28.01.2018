﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
   public LaunchBase1Script player1;
   public LaunchBase2Script player2;
   public Text score;
   public Text score2;

   // Update is called once per frame
   void Update()
   {
      if (player1.GetScore()<300)
      {
      score.text = "Player 1: " + player1.GetScore().ToString();
      }
      if (player1.GetScore()>300)
      {
         score.text = "PLAYER 1 WIN (" + player1.GetScore().ToString() + ")";
      }

      if (player2.GetScore2() < 300)
      {
         score2.text = "player 2: " + player2.GetScore2().ToString();
      }
      if (player2.GetScore2() > 300)
      {
         score2.text = "PLAYER 2 WIN!!! (" + player2.GetScore2().ToString() + ")";
      }
   }
}
